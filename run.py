# -*- coding: utf-8 -*-
import argparse

from src.download_videos import download_videos
from src.load_local_videos import load_local_videos
from src.split_into_frames import split_into_frames
from src.load_local_images import load_local_images
from src.preview_images import preview_images
import tensorflow as tf

from src.utils.config import SingleConfig
from src.update_label_info import update_label_info

if tf.test.gpu_device_name():
    from src.label import label
    from src.filter_images import filter_images
from src.match_backgrounds import match_backgrounds
from src.merge_jsons import merge_jsons
from src.train_test_split import train_test_split
from src.overview_dataset import overview_dataset
from src.preview_dataset import preview_dataset
from src.train import train
from src.validate import validate
from src.results_overviewer import ResultsOverviewer
from src.evaluate import evaluate
from src.demonstrate import demonstrate
from src.process_videos import process_videos
from src.profiler import Profiler
from src.utils.model_converter import ModelConverter
from src.utils.test_converted_models import ConvertedModelTester


if __name__ == "__main__":
    entry_points = {
        'download_videos': download_videos,
        'load_local_videos': load_local_videos,
        'split_into_frames': split_into_frames,
        'load_local_images': load_local_images,
        'preview_images': preview_images,
        'label': label if tf.test.gpu_device_name() else None,
        'update_label_info': update_label_info,
        'filter_images': filter_images if tf.test.gpu_device_name() else None,
        'match_backgrounds': match_backgrounds,
        'merge_jsons': merge_jsons,
        'train_test_split': train_test_split,
        'overview_dataset': overview_dataset,
        'preview_dataset': preview_dataset,
        'train': train,
        'validate': validate,
        'overview_results': ResultsOverviewer.run,
        'evaluate': evaluate,
        'demonstrate': demonstrate,
        'process_videos': process_videos,
        'profile': Profiler.run,
        'convert': ModelConverter.run,
        'test_converted_models': ConvertedModelTester.run,
    }
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('command', type=str, choices=entry_points.keys())
    args = parser.parse_args()

    submodule = args.command
    if submodule not in entry_points.keys():
        parser.print_help()

    config = SingleConfig()
    if config.validate_files():
        entry_points[submodule]()
