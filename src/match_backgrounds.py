import json
import random
from tqdm import tqdm
import tensorflow as tf

from src.utils.dataset import SegmentationDataset
from src.utils.miscellaneous import write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().match_backgrounds


def match_backgrounds():
    print('Loading background images')
    with open(config.backgrounds_json) as file:
        backgrounds_data = json.load(file)
    if config.backgrounds_count is not None:
        backgrounds_data = backgrounds_data[:config.backgrounds_count]
    backgrounds = tf.zeros(
        (len(backgrounds_data), *config.comparison_image_size[::-1], config.background_channels),
        dtype=tf.float32
    )
    for i, background_data in enumerate(tqdm(backgrounds_data)):
        background = tf.image.decode_jpeg(
            tf.io.read_file(background_data['image_filename']),
            channels=config.background_channels
        )
        background = tf.image.resize(background, config.comparison_image_size[::-1])
        backgrounds = tf.concat((backgrounds[:i], [background], backgrounds[i + 1:]), axis=0)

    print('Matching backgrounds')
    with open(config.original_dataset_json) as file:
        original_objects_data = json.load(file)
    random.seed(config.random_seed)
    result_objects_count = config.result_images_count \
        if isinstance(config.result_images_count, int) \
        else int(config.result_images_count * len(original_objects_data))
    original_objects_indices = list(range(len(original_objects_data)))
    random.shuffle(original_objects_indices)
    result_objects_indices = sorted(original_objects_indices[:result_objects_count])
    result_objects_data = [original_objects_data[i] for i in result_objects_indices]

    for result_object_data in tqdm(result_objects_data):
        image = tf.image.decode_jpeg(
            tf.io.read_file(result_object_data['image_filename']),
            channels=config.image_channels
        )
        image = tf.image.resize(image, config.comparison_image_size[::-1])
        mask_filenames = [mask_data['mask_filename'] for mask_data in result_object_data['masks']]
        if len(mask_filenames) == 0:
            result_object_data['background'] = random.choice(backgrounds_data)
            continue
        mask = SegmentationDataset.merge_instance_masks(mask_filenames, config.comparison_image_size[::-1], False, 1,
                                                        None, False, [None, None], False)
        mask_border = tf.where(tf.logical_and(
            tf.greater(mask, config.border_intensity_range[0]),
            tf.less(mask, config.border_intensity_range[1])
        ), 1., 0.)
        image_border = image * mask_border
        image_border_batch = tf.expand_dims(image_border, 0)
        image_border_batch = tf.repeat(image_border_batch, backgrounds.shape[0], axis=0)
        mask_border_batch = tf.expand_dims(mask_border, 0)
        mask_border_batch = tf.repeat(mask_border_batch, backgrounds.shape[0], axis=0)
        background_borders = backgrounds * mask_border_batch
        squared_errors = tf.square(image_border_batch - background_borders)
        mean_squared_errors = tf.reduce_mean(squared_errors, axis=(1, 2, 3))
        best_background_index = tf.argmin(mean_squared_errors).numpy()
        result_object_data['background'] = backgrounds_data[best_background_index]

    print('Saving result dataset jsons')
    write_json_data(config.result_dataset_json, result_objects_data)
    print('Done')


if __name__ == '__main__':
    match_backgrounds()
