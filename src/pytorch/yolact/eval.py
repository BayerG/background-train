# -*- coding: utf-8 -*-
import os

import torch
from tqdm import tqdm
from matplotlib import pyplot

from src.pytorch.data_loader.background_dataset import BackgroundDataset
from src.pytorch.business.mobilenet_mask_selector import MaskSelector
from src.utils.config import SingleConfig

cfg = SingleConfig().yolact


def main():
    with torch.no_grad():
        if not os.path.exists('results'):
            os.makedirs('results')

        # if cfg.use_GPU:
        #     cudnn.fastest = True
        #     torch.set_default_tensor_type('torch.cuda.FloatTensor')
        # else:
        #     torch.set_default_tensor_type('torch.FloatTensor')

        # data_loader = BackgroundDataset.get_loader(cfg.dataset_path, 'test', cfg.batch_size)
        # print('Loading model...', end='')
        # net = YolactMaskSelector()
        # print(' Done.')

        # cudnn.fastest = True
        # torch.set_default_tensor_type('torch.cuda.FloatTensor')

        data_loader = BackgroundDataset.get_loader('dataset/test_dataset.json', 1, (720, 1280), 0, 'cpu',
                                                   0.5, 0, 100, 10, 1.2, 0)

        print('Loading model...', end='')
        net = MaskSelector()
        net.load_state_dict(torch.load('experiments/2Jf8/st/mobilenet_best.pth.tar')['model_state_dict'])
        print(' Done.')

        for i in tqdm(range(0, 100)):
            image, label = data_loader.dataset[i]
            output = net(image.unsqueeze(0)).squeeze(0)
            if i == 0:
                print(f'Backbone output size: {net.backbone_output_size}')
            if i % 20 == 0:
                _, (p1, p2, p3) = pyplot.subplots(1, 3)
                p1.imshow(image.byte().cpu())
                p2.imshow(label.cpu())
                p3.imshow(output.cpu())


        print(f'Transform time: {net.transform_time / net.count * 1e+3 :.3f} ms\n'
              f'Inference time: {net.inference_time / net.count * 1e+3 :.3f} ms:\n'
              f'\tBackbone (MobileNet) time: {net.net.backbone_time / net.count * 1e+3 :.3f} ms\n'
              f'\tUpsample time: {net.net.upsample_time / net.count * 1e+3 :.3f} ms'
              )
