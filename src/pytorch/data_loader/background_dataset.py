# -*- coding: utf-8 -*-
import json
from pathlib import Path
import numpy as np
from PIL import Image

import torch
from torchvision.transforms.functional import resize, hflip, vflip, rotate, resized_crop
from torch.utils.data import Dataset, DataLoader


class BackgroundDataset(Dataset):
    def __init__(self, dataset_path, img_size, device, hflip_probability, vflip_probability,
                 max_offset, max_angle, max_scale, random_seed=None):
        self._path = Path(dataset_path)
        self._json_data = json.loads(self._path.open().read())
        self.img_size = img_size
        self.device = device
        self.hflip_probability = hflip_probability
        self.vflip_probability = vflip_probability
        self.max_offset = max_offset
        self.max_angle = max_angle
        self.max_scale = max_scale

        self.random_state = np.random.RandomState(random_seed)

    @staticmethod
    def get_both_loaders(config, device='cpu'):
        img_size = (config.img_height, config.img_width)
        train_loader = BackgroundDataset.get_loader(
            config.train_path, config.batch_size, img_size, 0, device,
            config['hflip_probability'], config['vflip_probability'], config['max_offset'],
            config['max_angle'], config['max_scale'], config['random_seed']
        )
        test_loader = BackgroundDataset.get_loader(
            config.test_path, config.batch_size, img_size, 0, device,
            config['hflip_probability'], config['vflip_probability'], config['max_offset'],
            config['max_angle'], config['max_scale'], config['random_seed']
        )
        return train_loader, test_loader

    @staticmethod
    def get_loader(dataset_path, batch_size, img_size, num_workers=0, device='cpu',
                   hflip_probability=0., vflip_probability=0., max_offset=0, max_angle=0., max_scale=1.,
                   random_seed=None):
        ds = BackgroundDataset(dataset_path, img_size, device, hflip_probability,
                               vflip_probability, max_offset, max_angle, max_scale, random_seed)
        return DataLoader(dataset=ds, batch_size=batch_size, shuffle=False, num_workers=num_workers)

    def __getitem__(self, index):
        image = self._transform_image(Image.open(self._json_data[index][0]).convert('RGB'), index)
        label = self._transform_image(Image.open(self._json_data[index][1]).convert('L'), index)
        return image, label

    def __len__(self):
        return len(self._json_data)

    def _transform_image(self, image, index):
        """
        Transforms PIL image to Tensor.
        """
        if self.img_size is not None:
            image = resize(image, self.img_size)
        if index % 2 == 1:
            image = hflip(image)
        if image.mode == 'RGB':
            image = np.array(image)[:, :, ::-1].copy()
        elif image.mode == 'L':
            image = np.array(image).copy() / 255
        image = torch.from_numpy(image)
        image = image.to(self.device)
        image = image.float()
        return image
