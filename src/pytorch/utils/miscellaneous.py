from collections import defaultdict

import torch

from src.pytorch.yolact.backbone import ResNetBackbone, VGGBackbone, ResNetBackboneGN, DarkNetBackbone

MEANS = (103.94, 116.78, 123.68)
STD   = (57.38, 57.12, 58.40)

COLORS = ((244,  67,  54),
          (233,  30,  99),
          (156,  39, 176),
          (103,  58, 183),
          ( 63,  81, 181),
          ( 33, 150, 243),
          (  3, 169, 244),
          (  0, 188, 212),
          (  0, 150, 136),
          ( 76, 175,  80),
          (139, 195,  74),
          (205, 220,  57),
          (255, 235,  59),
          (255, 193,   7),
          (255, 152,   0),
          (255,  87,  34),
          (121,  85,  72),
          (158, 158, 158),
          ( 96, 125, 139))

color_cache = defaultdict(lambda: {})

backbones = {
    'ResNetBackbone': ResNetBackbone,
    'VGGBackbone': VGGBackbone,
    'ResNetBackboneGN': ResNetBackboneGN,
    'DarkNetBackbone': DarkNetBackbone
}

activation_functions = {
    'tanh': torch.tanh,
    'sigmoid': torch.sigmoid,
    'softmax': lambda x: torch.nn.functional.softmax(x, dim=-1),
    'relu': lambda x: torch.nn.functional.relu(x, inplace=True),
    'none': lambda x: x,
}

losses = {
    'l1': torch.nn.L1Loss,
    'mse': torch.nn.MSELoss,
    'cross_entropy': torch.nn.CrossEntropyLoss
}

accuracies = {}
