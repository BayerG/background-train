from time import monotonic

from tqdm import tqdm

from src.preview_dataset import initialize_dataset
from src.train import initialize_model
from src.utils.miscellaneous import backbones
from src.utils.config import SingleConfig


class Profiler:
    def __init__(self):
        self._config = SingleConfig().profile

    @staticmethod
    def run():
        Profiler()._run()

    def _run(self):
        print('Profiling model')

        model = initialize_model(
            self._config.checkpoint,
            self._config.dataset.image_size,
            self._config.dataset.image_channels,
            self._config.dataset.batch_size,
            self._config.dataset.previous_frames,
            self._config.dataset.mask_channels,
            backbone=backbones[self._config.backbone.type],
            **{key: self._config.backbone[key] for key in self._config.backbone if key != 'type'},
            down_model_training=False,
            profile=True
        )

        dataset, dataset_length = initialize_dataset(
            self._config.dataset.json,
            self._config.sample_count,
            self._config.shuffle,
            self._config.random_seed,
            self._config.dataset.image_size,
            self._config.dataset.preserve_aspect_ratio,
            self._config.dataset.image_channels,
            self._config.dataset.mask_channels,
            self._config.dataset.background_channels,
            self._config.dataset.previous_frames,
            self._config.dataset.augmentations
        )
        dataset = dataset.repeat()
        dataset = dataset.batch(self._config.dataset.batch_size)

        call_count = 0

        for image, mask in dataset:
            if call_count >= self._config.measure_step:
                for name, value in zip(
                    ['Downscale', 'Preprocess', 'Backbone', 'Upscale', 'Activation'],
                    [model.downscale_time, model.preprocess_time, model.backbone_time, model.upscale_time,
                     model.activation_time]
                ):
                    print(f'{name}: {value / call_count * 1000:.2f} ms')
                print()

                model.downscale_time = 0
                model.preprocess_time = 0
                model.backbone_time = 0
                model.upscale_time = 0
                model.activation_time = 0
                call_count = 0

            model(image)
            call_count += 1
