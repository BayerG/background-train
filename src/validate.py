from src.train import get_dataset, initialize_model
from src.utils.miscellaneous import losses, \
    metrics, backbones
from src.utils.config import SingleConfig

config = SingleConfig().validate


def validate():
    print('Loading dataset')
    dataset, _ = get_dataset(
        config.dataset[config.dataset_type].json,
        None,
        config.dataset[config.dataset_type].augmentations,
    )

    loss = losses[config.loss.type](**{key: config.loss[key] for key in config.loss if key != 'type'})
    metric_objects = []
    for metric_info in config.metrics:
        metric = metrics[metric_info.type](**{key: metric_info[key] for key in metric_info if key != 'type'})
        metric_objects.append(metric)

    print('Initializing model')
    model = initialize_model(
        config.mask_selector,
        config.checkpoint,
        config.dataset.image_size,
        config.dataset.image_channels,
        config.dataset.batch_size,
        config.dataset.previous_frames,
        config.dataset.mask_channels,
        backbone=backbones[config.backbone.type],
        **{key: config.backbone[key] for key in config.backbone if key != 'type'},
        down_model_training=config.down_model_training
    )

    model.compile(loss=loss, metrics=metric_objects)

    model.evaluate(x=dataset, verbose=config.verbose)

    print('Done')


if __name__ == '__main__':
    validate()
