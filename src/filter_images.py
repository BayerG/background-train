import json

import cv2
from tqdm import tqdm
import torch

from src.label import initialize_model, predict_and_postprocess
from src.utils.miscellaneous import write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().filter_images
yolact_config = SingleConfig().yolact


def filter_images():
    print('Filtering images')
    with open(config.original_images_json) as file:
        original_images_data = json.load(file)
    result_images_data = []
    torch.backends.cudnn.fastest = True
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    print('Initializing model')
    model, transform = initialize_model(config.yolact_weights)
    class_index = yolact_config.dataset.class_names.index(config.instance_class_name)

    for original_image_data in tqdm(original_images_data):
        image = cv2.imread(original_image_data['image_filename'])
        classes, _, _, _ = predict_and_postprocess(image, model, transform, config.score_threshold)
        has_instance = False
        for class_ in classes:
            if class_ == class_index:
                has_instance = True
                break
        if not has_instance:
            result_images_data.append(original_image_data)

    print('Writing result json')
    write_json_data(config.result_images_json, result_images_data)
    print('Done')


if __name__ == '__main__':
    filter_images()
