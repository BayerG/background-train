import json
import random
from tqdm import tqdm

from src.utils.miscellaneous import write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().merge_jsons


def merge_jsons():
    random.seed(config.random_seed)

    print('Merging jsons')
    merged_data = []
    image_filenames = set()
    dublicate_count = 0
    for original in config.originals:
        with open(original['json']) as file:
            original_objects_data = json.load(file)
        sampled_objects_count = int(original.ratio * len(original_objects_data))
        sampled_objects_indices = sorted(random.sample(range(len(original_objects_data)), sampled_objects_count))
        sampled_objects_data = [original_objects_data[i] for i in sampled_objects_indices]
        for sampled_object_data in tqdm(sampled_objects_data, desc=f'{original["json"]} ({original.ratio * 100}%)'):
            if 'image_filename' in sampled_object_data and config.unique_image_filenames:
                if sampled_object_data['image_filename'] in image_filenames:
                    dublicate_count += 1
                    continue
                else:
                    image_filenames.add(sampled_object_data['image_filename'])
            merged_data.append(sampled_object_data)
    print(f'Skipped {dublicate_count} duplicates with same image filenames')

    print('Writing result json')
    write_json_data(config.result_json, merged_data)
    print('Done')


if __name__ == '__main__':
    merge_jsons()
