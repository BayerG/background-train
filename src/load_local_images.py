import shutil
import os
from tqdm import tqdm

from src.utils.miscellaneous import images_folder, new_subfolder, max_images_in_subfolder, write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().load_local_images


def load_local_images():
    print('Loading local images')
    filename_count = 0
    images_subfolder = new_subfolder(images_folder)
    subfolder_image_index = 0
    data = []
    original_filenames = []

    for dirname, _, filenames in os.walk(config.local_folder):
        for filename in filenames:
            original_filenames.append(os.path.join(dirname, filename))
            filename_count += 1
            if config.image_count is not None and filename_count >= config.image_count:
                break

    for original_filename in tqdm(original_filenames):
        if subfolder_image_index >= max_images_in_subfolder:
            images_subfolder = new_subfolder(images_folder)
            subfolder_image_index = 0
        if not config.move:
            destination_filename = shutil.copy2(original_filename, images_subfolder)
        else:
            destination_filename = shutil.move(original_filename, images_subfolder)
        data.append({
            'image_filename': destination_filename,
            'dataset_name': config.dataset_name
        })
        subfolder_image_index += 1

    print('Saving images json')
    write_json_data(config.images_json, data)
    print('Done')
