from matplotlib import pyplot as plt
from tqdm import tqdm
import tensorflow as tf

from src.preview_dataset import initialize_dataset
from src.train import initialize_model
from src.utils.miscellaneous import backbones, losses, metrics
from src.utils.config import SingleConfig

config = SingleConfig().evaluate


def evaluate():
    print('Evaluating model')
    dataset, dataset_length = initialize_dataset(
        config.dataset.json,
        config.objects_count,
        config.shuffle,
        config.random_seed,
        config.dataset.image_size,
        config.dataset.preserve_aspect_ratio,
        config.dataset.image_channels,
        config.dataset.mask_channels,
        config.dataset.background_channels,
        config.dataset.previous_frames,
        config.augmentations
    )

    model = initialize_model(config.checkpoint, config.dataset.image_size, config.dataset.image_channels,
                             1, config.dataset.previous_frames, config.dataset.mask_channels,
                             backbone=backbones[config.backbone.type],
                             **{key: config.backbone[key] for key in config.backbone if key != 'type'},
                             down_model_training=False)

    loss = losses[config.loss.type](**{key: config.loss[key] for key in config.loss if key != 'type'})
    metric_objects = []
    for metric_info in config.metrics:
        metric = metrics[metric_info.type](**{key: metric_info[key] for key in metric_info if key != 'type'})
        metric_objects.append(metric)

    objects_data = []

    for index, (image, mask) in enumerate(tqdm(dataset, total=min(config.objects_count, dataset_length),
                                               desc='Evaluating')):
        inputs = tf.expand_dims(image, 0)
        label = tf.expand_dims(mask, 0)
        result = model.predict(inputs)
        object_loss = loss(label, result)
        object_metrics = {}
        for metric_object in metric_objects:
            metric_object.reset_states()
            metric_object(label, result)
            object_metrics[metric_object.name] = metric_object.result()
        result = tf.squeeze(result, 0)
        if config.dataset.previous_frames:
            previous_mask = image[:, :, config.dataset.image_channels:]
            image = image[:, :, :config.dataset.image_channels]
        else:
            previous_mask = None
        objects_data.append((image, mask, previous_mask, result, object_loss, object_metrics))

    if config.top_negative is not None:
        objects_data.sort(key=lambda object_data: object_data[4], reverse=True)
        objects_data = objects_data[:config.top_negative]

    for image, mask, previous_mask, result, object_loss, object_metrics in tqdm(objects_data, desc='Visualizing'):
        if not config.dataset.previous_frames:
            _, (ax1, ax2, ax4) = plt.subplots(1, 3, figsize=(16, 6))
        else:
            _, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4, figsize=(20, 6))
        image = tf.cast(image, tf.uint8)
        ax1.imshow(image, cmap='gray')
        ax1.set_title('image')
        ax2.imshow(mask, cmap='gray')
        ax2.set_title('mask')
        if config.dataset.previous_frames:
            ax3.imshow(previous_mask, cmap='gray')
            ax3.set_title('previous mask')
        ax4.imshow(result)
        result_info = f'loss: {object_loss:.4f}'
        for metrics_name, metrics_value in object_metrics.items():
            result_info += f'\n{metrics_name}: {metrics_value:.4f}'
        ax4.set_title(result_info)
        plt.show()


if __name__ == '__main__':
    evaluate()
