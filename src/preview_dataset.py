import json
import random

from tqdm import tqdm
import tensorflow as tf
from matplotlib import pyplot as plt

from src.utils.dataset import SegmentationDataset
from src.utils.augmentations import Augmentations
from src.utils.miscellaneous import sample_data
from src.utils.config import SingleConfig

config = SingleConfig().preview_dataset


def preview_dataset():
    print('Previewing dataset')
    dataset, dataset_length = initialize_dataset(
        config.dataset.json,
        config.show_count,
        config.shuffle,
        config.random_seed,
        config.dataset.image_size,
        config.dataset.preserve_aspect_ratio,
        config.dataset.image_channels,
        config.dataset.mask_channels,
        config.dataset.background_channels,
        config.dataset.previous_frames,
        config.augmentations
    )

    for index, (image, mask) in enumerate(tqdm(dataset, total=min(config.show_count, dataset_length))):
        if not config.dataset.previous_frames:
            _, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 6))
        else:
            previous_mask = image[:, :, config.dataset.image_channels:]
            image = image[:, :, :config.dataset.image_channels]
            _, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 6))
        image = tf.cast(image, tf.uint8)
        ax1.imshow(image, cmap='gray')
        ax1.set_title('image')
        ax2.imshow(mask, cmap='gray')
        ax2.set_title('mask')
        if config.dataset.previous_frames:
            ax3.imshow(previous_mask, cmap='gray')
            ax3.set_title('previous mask')
        plt.show()

    print('Done')


def initialize_dataset(dataset_json, show_count, shuffle, random_seed, image_size, preserve_aspect_ratio,
                       image_channels, mask_channels, background_channels, previous_frames, augmentations):
    random.seed(random_seed)
    with open(dataset_json) as file:
        objects_data = json.load(file)

    print(f'Sampling {show_count} objects from dataset')
    sampled_data = sample_data(objects_data, show_count, shuffle, random_seed)
    dataset, dataset_length = SegmentationDataset.load_dataset(
        sampled_data,
        image_size[::-1],
        preserve_aspect_ratio,
        image_channels,
        mask_channels,
        background_channels,
        previous_frames
    )
    if augmentations:
        dataset = dataset.map(Augmentations(previous_frames, image_channels))
        
    return dataset, dataset_length 


if __name__ == '__main__':
    preview_dataset()
