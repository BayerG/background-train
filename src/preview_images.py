import json
import random

from tqdm import tqdm
import tensorflow as tf
from matplotlib import pyplot as plt

from src.utils.miscellaneous import sample_data
from src.utils.config import SingleConfig

config = SingleConfig().preview_images


def preview_images():
    random.seed(config.random_seed)

    print('Previewing images')

    with open(config.images_json) as file:
        images_data = json.load(file)

    print(f'Sampling {config.show_count} objects from images')
    sampled_data = sample_data(images_data, config.show_count, config.shuffle, config.random_seed)

    for image_data in tqdm(sampled_data):
        image = tf.image.decode_jpeg(tf.io.read_file(image_data['image_filename']))
        if 'previous_frame_filename' not in image_data:
            plt.imshow(image)
        else:
            previous_frame = tf.image.decode_jpeg(tf.io.read_file(image_data['previous_frame_filename']))
            _, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 6))
            ax1.imshow(previous_frame)
            ax1.set_title('previous')
            ax2.imshow(image)
            ax2.set_title('current')
        plt.show()

    print('Done')


if __name__ == '__main__':
    preview_images()
