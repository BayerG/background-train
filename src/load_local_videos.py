import os
import cv2
import shutil
from tqdm import tqdm

from src.utils.miscellaneous import videos_folder, new_subfolder, write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().load_local_videos


def load_local_videos():
    print('Loading local videos')
    videos_subfolder = new_subfolder(videos_folder)
    data = []
    original_filenames = []

    for dirname, _, filenames in os.walk(config.local_folder):
        for filename in filenames:
            original_filenames.append(os.path.join(dirname, filename))
    for video_index, original_filename in enumerate(tqdm(original_filenames)):
        cap = cv2.VideoCapture(original_filename)
        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        fps = cap.get(cv2.CAP_PROP_FPS)
        if frame_count == 0 and fps == 0:
            print('Video load error')
            continue
        video_length = frame_count / fps
        filename_extension = os.path.splitext(original_filename)[1]
        destination_filename = os.path.join(videos_subfolder, str(video_index) + filename_extension)
        if not config.move:
            shutil.copy2(original_filename, destination_filename)
        else:
            destination_filename = shutil.move(original_filename, destination_filename)
        data.append({
            'video_filename': destination_filename,
            'video_length': video_length,
            'fps': fps,
            'dataset_name': config.dataset_name
        })

    print('Saving videos json')
    write_json_data(config.videos_json, data)
    print('Done')
