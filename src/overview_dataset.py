from datetime import datetime
import json
import os
from collections import defaultdict

from matplotlib import pyplot as plt
from tqdm import tqdm
import numpy as np
import tensorflow as tf

from src.utils.dataset import SegmentationDataset
from src.utils.miscellaneous import sample_data, scale_box
from src.utils.config import SingleConfig

config = SingleConfig().overview_dataset


def overview_dataset():
    print('Overviewing dataset')
    with open(config.dataset.json) as file:
        objects_data = json.load(file)

    _, dataset_length = SegmentationDataset.load_dataset(
        objects_data,
        config.dataset.image_size[::-1],
        config.dataset.preserve_aspect_ratio,
        config.dataset.image_channels,
        config.dataset.mask_channels,
        config.dataset.background_channels,
        config.dataset.previous_frames
    )
    print(f'Total dataset length: {dataset_length}')

    features = defaultdict(list)
    for object_data in tqdm(objects_data, desc='Calculating features from json'):
        for mask_data in object_data['masks']:
            features['mask_scores'].append(mask_data['score'])
            left, top, right, bottom = scale_box(
                mask_data['box'],
                (
                    mask_data.get('mask_height', config.dataset.image_size[0]),
                    mask_data.get('mask_width', config.dataset.image_size[1])
                ),
                config.dataset.image_size[::-1]
            )
            width = right - left
            height = bottom - top
            aspect_ratio = width / height
            area = width * height
            for box_feature_value, box_feature in zip(
                    [left, top, right, bottom, width, height, aspect_ratio, area],
                    ['mask_box_lefts', 'mask_box_tops', 'mask_box_rights', 'mask_box_bottoms',
                     'mask_box_widths', 'mask_box_heights', 'mask_box_aspect_ratios', 'mask_box_areas']
            ):
                features[box_feature].append(box_feature_value)
        features['mask_counts'].append(len(object_data.get('masks', [])))
        features['original_image_width'].append(object_data.get('image_width', 0))
        features['original_image_height'].append(object_data.get('image_height', 0))
        features['video_lengths'].append(object_data.get('video_length', 0))
        features['fpss'].append(object_data.get('fps', 0))
        features['dataset_names'].append(object_data.get('dataset_name', ''))
        features['total_images_in_videos'].append(object_data.get('total_images_in_video', 0))
        features['syntetic_background'].append(str('background' in object_data))

    print(f'Sampling {config.sample_count} objects from dataset with length {len(objects_data)}')
    sampled_objects_data = sample_data(objects_data, config.sample_count, False, config.random_seed)
    dataset, dataset_length = SegmentationDataset.load_dataset(
        sampled_objects_data,
        config.dataset.image_size[::-1],
        config.dataset.preserve_aspect_ratio,
        config.dataset.image_channels,
        config.dataset.mask_channels,
        config.dataset.background_channels,
        config.dataset.previous_frames
    )
    for image, mask in tqdm(dataset, desc='Calculating features from data', total=dataset_length):
        features['image_min_intensity'].append(tf.reduce_min(image).numpy())
        features['image_max_intensity'].append(tf.reduce_max(image).numpy())
        features['image_mean_intensity'].append(tf.reduce_mean(image).numpy())
        dx, dy = tf.image.image_gradients(tf.expand_dims(image, 0))
        gradient_magnitude = tf.squeeze(tf.sqrt(dx**2 + dy**2), 0)
        features['image_min_gradient'].append(tf.reduce_min(gradient_magnitude).numpy())
        features['image_max_gradient'].append(tf.reduce_max(gradient_magnitude).numpy())
        features['image_mean_gradient'].append(tf.reduce_sum(gradient_magnitude).numpy())
        features['mask_area'].append(tf.reduce_sum(tf.where(tf.less(mask, config.mask_area_threshold), 0, 1)).numpy())

    for feature in tqdm(features, desc='Generating plots'):
        figure = plt.figure()
        if feature in config.bins:
            bins = parse_bins(config.bins[feature])
        else:
            bins = None
        plt.hist(features[feature], bins=bins)
        plt.title(feature)
        plt.show()

    print('Done')


def parse_bins(bins):
    if bins is None:
        return None
    if 'range' in bins:
        return np.arange(*bins['range'])
    elif 'linspace' in bins:
        return np.linspace(*bins['linspace'])
    else:
        return None


if __name__ == '__main__':
    overview_dataset()
