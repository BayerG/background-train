import json
import os
import torch
import cv2
from tqdm import tqdm

from src.pytorch.yolact.augmentations import FastBaseTransform
from src.pytorch.yolact.net import Yolact
from src.pytorch.yolact.postprocessing import postprocess
from src.utils.miscellaneous import masks_folder, max_images_in_subfolder, new_subfolder, write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().label
yolact_config = SingleConfig().yolact


def label():
    print('Labelling images')
    with open(config.images_json) as file:
        images_data = json.load(file)

    torch.backends.cudnn.fastest = True
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    print('Initializing model')
    model, transform = initialize_model(config.yolact_weights)
    class_index = yolact_config.dataset.class_names.index(config.instance_class_name)
    masks_subfolder = new_subfolder(masks_folder)
    mask_index = 0
    objects_data = []
    progress_bar = tqdm(images_data)

    for image_data in progress_bar:
        if 'dataset_name' in image_data:
            progress_bar.set_description(image_data['dataset_name'])
        else:
            progress_bar.set_description('')
        object_data, mask_index, masks_subfolder = process_image(image_data, 'image_filename', '', model, transform,
                                                                 class_index, mask_index, masks_subfolder)
        if 'previous_frame_filename' in image_data:
            previous_object_data, mask_index, masks_subfolder = process_image(image_data, 'previous_frame_filename',
                                                                              'previous_', model, transform,
                                                                              class_index, mask_index, masks_subfolder)
            object_data.update(previous_object_data)
        object_data.update(image_data)
        objects_data.append(object_data)

    progress_bar.close()
    print('Saving dataset json')
    write_json_data(config.dataset_json, objects_data)
    print('Done')


def process_image(image_data, image_key, prefix, model, transform, class_index, mask_index, masks_subfolder):
    object_data = {
        prefix + 'masks': [],
        prefix + 'instance_count': 0,
        prefix + 'instance_class_name': config.instance_class_name,
    }
    image = cv2.imread(image_data[image_key])
    object_data['image_height'], object_data['image_width'], object_data['image_channels'] = image.shape
    classes, scores, boxes, masks = predict_and_postprocess(image, model, transform, config.score_threshold)

    for i in range(len(classes)):
        if classes[i] != class_index:
            continue
        if mask_index >= max_images_in_subfolder:
            masks_subfolder = new_subfolder(masks_folder)
            mask_index = 0
        mask_filename = os.path.join(masks_subfolder, f'{mask_index}.jpg')
        mask = masks[i]

        thresholded_mask = torch.where(
            torch.le(mask, config.mask_area_threshold),
            torch.zeros(mask.shape),
            torch.ones(mask.shape)
        )
        instance_area = torch.sum(thresholded_mask)

        mask = mask * 255
        mask = mask.reshape(mask.shape[0], mask.shape[1], 1)
        mask = mask.byte().cpu().numpy()
        mask_height, mask_width, mask_channels = mask.shape
        cv2.imwrite(mask_filename, mask)
        object_data[prefix + 'masks'].append({
            'mask_filename': mask_filename,
            'mask_width': mask_width,
            'mask_height': mask_height,
            'mask_channels': mask_channels,
            'instance_area': instance_area.cpu().item(),
            'score': scores[i].cpu().item(),
            'box': boxes[i].cpu().numpy().tolist()
        })
        mask_index += 1

    return object_data, mask_index, masks_subfolder


def initialize_model(yolact_weights_filename):
    transform = FastBaseTransform()
    model = Yolact()
    model.load_weights(yolact_weights_filename)
    model.eval()
    return model, transform


def predict_and_postprocess(image, model, transform, score_threshold):
    with torch.no_grad():
        height, width, _ = image.shape
        image = torch.from_numpy(image)
        image = image.cuda()
        transformed_image = image.float()
        transformed_image = transformed_image.unsqueeze(0)
        transformed_image = transform(transformed_image)
        prediction = model(transformed_image)
        classes, scores, boxes, masks \
            = postprocess(prediction, width, height,
                          score_threshold=score_threshold)
    return classes, scores, boxes, masks


if __name__ == '__main__':
    label()
