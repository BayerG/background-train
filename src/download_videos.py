import pytube
from pytube.exceptions import RegexMatchError
from tqdm import tqdm
import os

from src.utils.miscellaneous import videos_folder, new_subfolder, write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().download_videos


def download_videos():
    print('Downloading videos')
    videos_subfolder = new_subfolder(videos_folder)
    data = []

    with open(config.url_list_filename) as file:
        for url in file:
            print(f'Downloading video from {url}')
            try:
                youtube = pytube.YouTube(url)
            except RegexMatchError:
                print(f'Url {url} parsing error')
                continue
            video = youtube.streams.get_highest_resolution()
            video.download(videos_subfolder)
            data.append({
                'video_filename': os.path.join(videos_subfolder, video.default_filename),
                'video_url': youtube.watch_url,
                'video_length': youtube.length,
                'video_title': youtube.title,
                'fps': video.fps,
                'dataset_name': config.dataset_name
            })

    print('Saving videos json')
    write_json_data(config.videos_json, data)
    print('Done')


if __name__ == '__main__':
    download_videos()
