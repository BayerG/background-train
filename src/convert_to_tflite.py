import tensorflow as tf

from src.utils.config import SingleConfig
from src.train import initialize_model
from src.utils.miscellaneous import backbones

config = SingleConfig().convert_to_tflite


def convert_to_tflite():
    print('Loading model')
    model = initialize_model(config.original_model_path, config.image_size, config.image_channels,
                             1, config.previous_frames, config.mask_channels,
                             backbone=backbones[config.backbone.type],
                             **{key: config.backbone[key] for key in config.backbone if key != 'type'})
    print('Converting to tflite model')
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    tflite_model = converter.convert()
    print('Saving tflite model')
    with open(config.tflite_model_path, 'wb') as file:
        file.write(tflite_model)


if __name__ == '__main__':
    convert_to_tflite()
