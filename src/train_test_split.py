import json
import sklearn.model_selection

from src.utils.config import SingleConfig

config = SingleConfig().train_test_split


def train_test_split():
    print('Splitting dataset...', sep='')
    with open(config.original_json, 'r') as original_json:
        data = json.load(original_json)
    train_list, test_list = sklearn.model_selection.train_test_split(
        data,
        train_size=config.train_size,
        test_size=config.test_size,
        shuffle=config.shuffle,
        random_state=config.random_seed
    )
    with open(config.train_json, 'w') as train_json:
        json.dump(train_list, train_json, indent=4)
    with open(config.test_json, 'w') as test_json:
        json.dump(test_list, test_json, indent=4)
    print(' Done.')


if __name__ == '__main__':
    train_test_split()
