import json
import math

from tqdm import tqdm
import cv2
import os

from src.utils.miscellaneous import images_folder, new_subfolder, write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().split_into_frames


def split_into_frames():
    print('Splitting videos into frames')
    with open(config.videos_json) as file:
        videos_data = json.load(file)
    images_data = []

    for video_index, video_data in enumerate(videos_data):
        print(f'Processing {video_data["video_filename"]}')
        cap = cv2.VideoCapture(video_data['video_filename'])
        total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        fps = cap.get(cv2.CAP_PROP_FPS)
        images_subfolder = new_subfolder(images_folder)
        previous_image_index = None
        image_index = 0
        added_indices = []
        previous_frame = None

        if config.frame_recency is not None and config.frames_per_video is not None:
            raise ValueError('Either frame_recency or frames_per_video must be None')
        elif config.frame_recency is not None:
            frame_recency = config.frame_recency
        elif config.frames_per_video is not None:
            frame_recency = total_frames / config.frames_per_video / fps
        else:
            raise ValueError('Either frame_recency or frames_per_video must be set')

        progress_bar = tqdm(total=math.ceil(total_frames / (frame_recency * fps)), desc=f'({video_index + 1}/{len(videos_data)})')

        for frame_index in range(total_frames):
            success, image = cap.read()
            if not success:
                print('Video read error')
                continue

            if previous_image_index is None or frame_index >= previous_image_index + frame_recency * fps:
                progress_bar.update()
                image_filename = os.path.join(images_subfolder, f'{image_index}.jpg')
                cv2.imwrite(image_filename, image)
                image_data = {
                    'image_filename': image_filename,
                    'image_index': image_index,
                    'frame_index': frame_index,
                    'frame_recency': frame_recency if previous_image_index is not None else None,
                }
                if config.store_previous_frames and previous_frame is not None:
                    previous_frame_filename = os.path.join(images_subfolder, f'{image_index}p.jpg')
                    cv2.imwrite(previous_frame_filename, previous_frame)
                    image_data['previous_frame_filename'] = previous_frame_filename
                image_data.update(video_data)
                images_data.append(image_data)
                added_indices.append(len(images_data) - 1)
                previous_image_index = frame_index
                image_index += 1

            if config.store_previous_frames:
                previous_frame = image

        for added_index in added_indices:
            images_data[added_index]['total_images_in_video'] = image_index
        cap.release()

    print('Saving images json')
    write_json_data(config.images_json, images_data)
    print('Done')


if __name__ == '__main__':
    split_into_frames()
