import json
import tensorflow as tf
from tqdm import tqdm

from src.utils.miscellaneous import write_json_data
from src.utils.config import SingleConfig

config = SingleConfig().update_label_info


def update_label_info():
    print('Updating label info')
    with open(config.original_dataset_json) as file:
        objects_data = json.load(file)

    for object_data in tqdm(objects_data):
        image = tf.image.decode_jpeg(tf.io.read_file(object_data['image_filename']))
        object_data['image_height'], object_data['image_width'], object_data['image_channels'] = image.shape
        process_masks_data(object_data['masks'])
        if 'previous_masks' in object_data:
            process_masks_data(object_data['previous_masks'])

    print('Writing result json')
    write_json_data(config.result_dataset_json, objects_data)
    print('Done')


def process_masks_data(masks_data):
    for mask_data in masks_data:
        instance_mask = tf.image.decode_jpeg(tf.io.read_file(mask_data['mask_filename']),
                                             channels=config.mask_channels)
        mask_data['mask_height'], mask_data['mask_width'], mask_data['mask_channels'] = instance_mask.shape
        instance_mask = tf.cast(instance_mask, tf.float32)
        instance_mask /= 255
        instance_mask = tf.where(tf.less(instance_mask, config.mask_area_threshold), 0, 1)
        mask_data['instance_area'] = float(tf.reduce_sum(instance_mask))
