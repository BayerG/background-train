import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from tqdm import tqdm

from src.train import initialize_model
from src.preview_dataset import initialize_dataset
from src.utils.miscellaneous import backbones
from src.utils.config import SingleConfig
from src.utils.metrics import BinaryMeanIoU


class ResultsOverviewer:
    def __init__(self):
        self._config = SingleConfig().overview_results

    @staticmethod
    def run():
        ResultsOverviewer()._run()

    def _run(self):
        print('Overviewing results')
        model = initialize_model(
            self._config.checkpoint,
            self._config.dataset.image_size,
            self._config.dataset.image_channels,
            self._config.dataset.batch_size,
            self._config.dataset.previous_frames,
            self._config.dataset.mask_channels,
            backbone=backbones[self._config.backbone.type],
            **{key: self._config.backbone[key] for key in self._config.backbone if key != 'type'},
            down_model_training=False
        )

        for dataset_type in ['train', 'test']:
            dataset, dataset_length = initialize_dataset(
                self._config.dataset[dataset_type].json,
                self._config.sample_count,
                self._config.shuffle,
                self._config.random_seed,
                self._config.dataset.image_size,
                self._config.dataset.preserve_aspect_ratio,
                self._config.dataset.image_channels,
                self._config.dataset.mask_channels,
                self._config.dataset.background_channels,
                self._config.dataset.previous_frames,
                self._config.dataset[dataset_type].augmentations
            )

            dataset = dataset.batch(self._config.dataset.batch_size)

            mask_histogram = tf.zeros((self._config.probability_nbins,), dtype=tf.int32)
            result_histogram = tf.zeros((self._config.probability_nbins,), dtype=tf.int32)

            ious = [BinaryMeanIoU(2, threshold=threshold)
                    for threshold in np.linspace(0, 1, self._config.iou_nbins)]

            probability_xs = tf.linspace(0, 1, self._config.probability_nbins)
            iou_xs = tf.linspace(0, 1, self._config.iou_nbins)
            probability_bin_width = 1 / (self._config.probability_nbins - 1)
            iou_bin_width = 1 / (self._config.iou_nbins - 1)

            for image, mask in tqdm(dataset, total=dataset_length // self._config.dataset.batch_size):
                mask_histogram += tf.histogram_fixed_width(mask, (0, 1), self._config.probability_nbins)
                result = model.predict(image)
                result_histogram += tf.histogram_fixed_width(result, (0, 1), self._config.probability_nbins)
                for iou in ious:
                    iou.update_state(mask, result)

            mask_histogram = tf.cast(mask_histogram, tf.float32)
            result_histogram = tf.cast(result_histogram, tf.float32)

            plt.bar(probability_xs, tf.math.log(mask_histogram) - 1, width=probability_bin_width)
            plt.title(f'{dataset_type.title()} mask log probabilities')
            plt.show()

            plt.bar(probability_xs, tf.math.log(result_histogram) - 1, width=probability_bin_width)
            plt.title(f'{dataset_type.title()} result log probabilities')
            plt.show()

            plt.bar(iou_xs, [iou.result() for iou in ious], width=iou_bin_width)
            plt.title(f'{dataset_type.title()} IoUs')
            plt.show()
