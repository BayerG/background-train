# -*- coding: utf-8 -*-
import json
import cv2
import os

import numpy as np
from tqdm import tqdm

from src.mobilenet.mask_selector import process
from src.utils.miscellaneous import videos_folder, new_subfolder, write_json_data, backbones
from src.utils.config import SingleConfig
from src.train import initialize_model

config = SingleConfig().process_videos


def process_videos():
    print('Loading model')
    model = initialize_model(config.model_path, config.inference_image_size, config.image_channels,
                             1, config.previous_frames, config.mask_channels,
                             backbone=backbones[config.backbone.type],
                             **{key: config.backbone[key] for key in config.backbone if key != 'type'},
                             down_model_training=False)
    print('Processing videos')
    with open(config.original_videos_json) as file:
        videos_data = json.load(file)
    videos_subfolder = new_subfolder(videos_folder)
    fourcc = cv2.VideoWriter_fourcc(*config.result_video_codec)
    for video_index, video_data in enumerate(videos_data):
        print(f'({video_index + 1}/{len(videos_data)}) Processing {video_data["video_filename"]}')
        cap = cv2.VideoCapture(video_data['video_filename'])
        fps = cap.get(cv2.CAP_PROP_FPS)
        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        image_size = (
            int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        )
        result_video_filename = os.path.join(videos_subfolder, f'{video_index}.mp4')
        out = cv2.VideoWriter(result_video_filename, fourcc, fps, image_size)
        previous_mask = np.zeros((*config.inference_image_size[::-1], config.mask_channels), dtype='uint8')

        for _ in tqdm(range(frame_count)):
            success, frame = cap.read()
            if not success:
                print('Video read error')
                break
            frame = cv2.resize(frame, tuple(config.inference_image_size))
            frame, mask = process(frame, model, config.threshold, config.background_color,
                                  config.background_transparency, config.image_channels,
                                  previous_mask if config.previous_frames else None)
            frame = cv2.resize(frame, image_size)
            out.write(frame)
            previous_mask = mask * 255
            previous_mask = previous_mask.astype('uint8')

        video_data['original_video_filename'] = video_data['video_filename']
        video_data['video_filename'] = result_video_filename
        out.release()
        cap.release()

    print('Writing result json')
    write_json_data(config.result_videos_json, videos_data)
    print('Done')


if __name__ == '__main__':
    process_videos()
