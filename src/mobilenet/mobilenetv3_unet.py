from typing import Optional

import tensorflow as tf

from src.mobilenet.mobilenetv3 import Bneck, _make_divisible
from src.utils.config import SingleConfig

config = SingleConfig().mobilenet


class UpsampleBlock(tf.keras.layers.Layer):
    def __init__(self,
                 out_channels: int,
                 exp_channels: int,
                 kernel_size: int,
                 stride: int,
                 use_se: bool,
                 act_layer: str,
                 momentum: float,
                 l2_reg: float=1e-5,
                 name: str = 'UpsampleBlock'):
        super().__init__(name=name)

        self.conv_transpose = tf.keras.layers.Conv2DTranspose(
            out_channels,
            kernel_size=3 if stride == 1 else 4,
            strides=stride,
            kernel_regularizer=tf.keras.regularizers.l2(l2_reg),
            padding='same'
        )
        self.bottleneck = Bneck(
            out_channels,
            exp_channels,
            kernel_size,
            1,
            use_se,
            act_layer,
            momentum
        )

    def call(self, inputs, **kwargs):
        inputs1, inputs2 = inputs
        x = self.conv_transpose(inputs2)
        x = tf.concat((inputs1, x), 3)
        x = self.bottleneck(x)
        return x


class MobileNetV3Unet(tf.keras.Model):
    def __init__(self,
                 weights: str = 'imagenet',
                 down_model_training: Optional[bool] = None,
                 name: str = 'MobileNetUnet',
                 divisible_by: int = 8,
                 momentum: float = 0.99,
                 l2_reg: float = 1e-5):
        from src.utils.miscellaneous import base_models
        super().__init__(name=name)

        self.down_model_training = down_model_training

        self.up_blocks_settings = config.up_blocks

        self.feature_indices = []
        for setting in self.up_blocks_settings:
            self.feature_indices.append(setting[0])

        base_model = base_models[config.base_model](include_top=False, weights=weights)
        base_model_outputs = [base_model.get_layer(layer_name).output for layer_name in config.down_layers]
        self.down_model = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)

        self.up_blocks = []
        for up_block_index, (k, exp, out, SE, NL, s) in enumerate(self.up_blocks_settings):
            out_channels = _make_divisible(out * config.alpha, divisible_by)
            exp_channels = _make_divisible(exp * config.alpha, divisible_by)
            upsample_block = UpsampleBlock(out_channels, exp_channels, k, s, SE, NL, momentum,
                                           name=f'UpBlock{up_block_index}')
            self.up_blocks.append(upsample_block)

        self.output_layer = tf.keras.layers.Conv2D(1, 1, name='OutputLayer')

    def call(self, inputs, **kwargs):
        down_model_kwargs = kwargs.copy()
        if self.down_model_training is not None:
            down_model_kwargs['training'] = self.down_model_training

        down_features = self.down_model(inputs, **down_model_kwargs)
        x = down_features[-1]
        down_features = reversed(down_features[:-1])

        for up_block, down_feature in zip(self.up_blocks, down_features):
            x = up_block((down_feature, x), **kwargs)

        x = self.output_layer(x, **kwargs)

        return x
