from time import monotonic

import cv2
import numpy as np
import tensorflow as tf

from src.utils.resizer import Resizer


def process(image, processor, threshold, background, background_transparency, image_channels, previous_mask):
    inputs = image.copy()
    if image_channels == 1:
        inputs = cv2.cvtColor(inputs, cv2.COLOR_BGR2GRAY)
    else:
        inputs = cv2.cvtColor(inputs, cv2.COLOR_BGR2RGB)
    inputs = inputs.reshape(inputs.shape[0], inputs.shape[1], image_channels)
    if previous_mask is not None:
        inputs = np.concatenate((inputs, previous_mask), axis=2)
    inputs = np.expand_dims(inputs, 0)
    if isinstance(processor, tf.keras.Model):
        mask = processor.predict(inputs)
    elif isinstance(processor, tf.lite.Interpreter):
        input_details = processor.get_input_details()
        output_details = processor.get_output_details()
        inputs = inputs.astype('float32')
        processor.set_tensor(input_details[0]['index'], inputs)
        processor.invoke()
        mask = processor.get_tensor(output_details[0]['index'])
    else:
        raise ValueError('Incorrect processor type')
    mask = mask.squeeze(0)
    if threshold is not None:
        mask = np.where(np.less(mask, threshold), 0., 1.)
    if background is None:
        background = [0, 0, 0]
    background = np.array(background)
    image = mask * image + (1 - mask) * (image * background_transparency + background * (1 - background_transparency))
    image = image.astype('uint8')
    return image, mask


class MaskSelector(tf.keras.Model):
    def __init__(self,
                 backbone,
                 inference_size,
                 preprocess=True,
                 activation='sigmoid',
                 preserve_aspect_ratio=False,
                 profile=False,
                 *args, **kwargs):
        super().__init__()
        self.backbone = backbone(*args, **kwargs)
        self.inference_size = inference_size
        self.preprocess = preprocess
        self.preserve_aspect_ratio = preserve_aspect_ratio
        self.profile = profile

        self.downscale_time = 0
        self.preprocess_time = 0
        self.backbone_time = 0
        self.upscale_time = 0
        self.activation_time = 0

    def call(self, inputs, **kwargs):
        if self.profile:
            time = monotonic()

        _, height, width, _ = inputs.shape

        if not self.preserve_aspect_ratio:
            x = tf.image.resize(inputs, self.inference_size)
        else:
            x, resize_info = Resizer.downscale(inputs, self.inference_size)

        if self.profile:
            self.downscale_time += monotonic() - time
            time = monotonic()

        x = tf.cast(x, tf.float32)
        if self.preprocess:
            x = tf.keras.applications.mobilenet.preprocess_input(x)

        if self.profile:
            self.preprocess_time += monotonic() - time
            time = monotonic()

        x = self.backbone(x)

        if self.profile:
            self.backbone_time += monotonic() - time
            time = monotonic()

        if not self.preserve_aspect_ratio:
            x = tf.image.resize(x, (height, width))
        else:
            x = tf.image.resize(x, self.inference_size)
            x = Resizer.upscale(x, resize_info)

        if self.profile:
            self.upscale_time += monotonic() - time
            time = monotonic()

        x = tf.keras.activations.sigmoid(x)

        if self.profile:
            self.activation_time += monotonic() - time

        return x
