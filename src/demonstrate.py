# -*- coding: utf-8 -*-
import time

import cv2
import numpy as np
import tensorflow as tf

from src.mobilenet.mask_selector import process
from src.utils.miscellaneous import backbones
from src.utils.config import SingleConfig
from src.train import initialize_model

config = SingleConfig().demonstrate


def demonstrate():
    print('Demonstrating')
    print('Loading model')
    if config.model_path is None or str(config.model_path).endswith('ckpt'):
        processor = initialize_model(config.mask_selector, config.model_path, config.inference_image_size, config.image_channels,
                                 1, config.previous_frames, config.mask_channels,
                                 backbone=backbones[config.backbone.type],
                                 **{key: config.backbone[key] for key in config.backbone if key != 'type'},
                                 down_model_training=False)
    elif str(config.model_path).endswith('tflite'):
        processor = tf.lite.Interpreter(model_path=str(config.model_path))
        processor.allocate_tensors()
    else:
        raise ValueError('Incorrect model type')

    print('Capturing video device')
    cap = cv2.VideoCapture(config.video_device)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, config.input_image_size[0])
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, config.input_image_size[1])

    previous_mask = np.zeros((*config.inference_image_size[::-1], config.mask_channels), dtype='uint8')

    start_time = time.monotonic()
    frames_count = 0

    while cap.isOpened():
        if config.profiling:
            if frames_count == 100:
                print(f'fps: {frames_count / (time.monotonic() - start_time)}')
                start_time = time.monotonic()
                frames_count = 0

            frames_count += 1

        success, frame = cap.read()
        if not success:
            print('Video device error')
            break
        frame = cv2.resize(frame, tuple(config.inference_image_size))
        if config.horizontal_flip:
            frame = cv2.flip(frame, 1)
        if config.vertical_flip:
            frame = cv2.flip(frame, 0)

        frame, mask = process(frame, processor, config.threshold, config.background_color,
                              config.background_transparency, config.image_channels,
                              previous_mask if config.previous_frames else None)
        frame = cv2.resize(frame, tuple(config.output_image_size))
        cv2.imshow('result', frame)

        previous_mask = mask * 255
        previous_mask = previous_mask.astype('uint8')

        key = cv2.waitKey(1)
        if key == 27:
            break

    cap.release()
    cv2.destroyAllWindows()
    print('Done')


if __name__ == '__main__':
    demonstrate()
