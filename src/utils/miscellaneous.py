import random
import os
import json
from typing import *

import tensorflow as tf
import tensorflow_addons as tfa

import src.mobilenet.mask_selector
import src.deeplab.mask_selector
from src.utils.losses import BorderGaussianLoss, AsymmetricalMSE
from src.utils.metrics import MaskAccuracy, BorderAccuracy, BinaryMeanIoU
from src.mobilenet.mobilenetv3_unet import MobileNetV3Unet


videos_folder = 'dataset/data/videos'
images_folder = 'dataset/data/images'
masks_folder = 'dataset/data/masks'
max_images_in_subfolder = 1000
cache_filename = 'cache/cache'
overviews_folder = 'overviews'
previews_folder = 'previews'
experiments_folder = 'experiments'


def new_subfolder(path):
    if not os.path.exists(path):
        os.makedirs(path)
    folder_names = os.listdir(path)
    max_folder_index = -1
    for folder_name in folder_names:
        try:
            folder_index = int(folder_name)
            if folder_index > max_folder_index:
                max_folder_index = folder_index
        except ValueError:
            pass
    folder_name = str(max_folder_index + 1)
    folder_path = os.path.join(path, folder_name)
    os.mkdir(folder_path)
    return folder_path


def write_json_data(filename, data, indent=4):
    folder_name = os.path.dirname(filename)
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    with open(filename, 'w') as file:
        json.dump(data, file, indent=indent)


def between(value, range_, whole=1):
    left = None
    right = None
    if range_[0] is not None:
        left = range_[0] if isinstance(range_[0], int) else range_[0] * whole
    if range_[1] is not None:
        right = range_[1] if isinstance(range_[1], int) else range_[1] * whole
    if left is not None and right is not None:
        return left <= value <= right
    elif left is not None:
        return left <= value
    elif right is not None:
        return value <= right
    else:
        return True


def sample_data(data, count, shuffle, random_seed):
    random.seed(random_seed)
    objects_indices = list(range(len(data)))
    sample_indices = random.sample(objects_indices, min(count, len(objects_indices)))
    if not shuffle:
        sample_indices = sorted(sample_indices)
    sampled_data = [data[i] for i in sample_indices]
    return sampled_data


def scale_box(box, original_image_size, new_image_size):
    """
    Scales the bounding box to new image size.
    :param box: A list [left, top, right, bottom].
    :param original_image_size: A list or tuple (height, width).
    :param new_image_size: A list or tuple (height, width).
    :return: Scaled bounding box as list [left, top, right, bottom].
    """
    original_image_height, original_image_width = original_image_size
    new_image_height, new_image_width = new_image_size
    left, top, right, bottom = box
    new_left = left * new_image_width / original_image_width
    new_right = right * new_image_width / original_image_width
    new_top = top * new_image_height / original_image_height
    new_bottom = bottom * new_image_height / original_image_height
    return [new_left, new_top, new_right, new_bottom]


class LearningRateLogger(tf.keras.callbacks.Callback):
    def __init__(self):
        super().__init__()
        self.step = 0

    def on_train_batch_begin(self, batch, logs=None):
        self.step += 1

        if tf.is_tensor(self.model.optimizer.learning_rate):
            tf.summary.scalar(
                'batch_learning_rate',
                tf.keras.backend.get_value(self.model.optimizer.learning_rate),
                self.step
            )
        elif isinstance(self.model.optimizer.learning_rate, tf.keras.optimizers.schedules.LearningRateSchedule):
            step = self.model.optimizer.iterations
            step = tf.cast(step, tf.float32)
            tf.summary.scalar(
                'batch_learning_rate',
                tf.keras.backend.get_value(self.model.optimizer.learning_rate(step)),
                self.step
            )


def get_compatible_schedule_class(schedule_class: Type[tf.keras.optimizers.schedules.LearningRateSchedule]):
    class LearningRateSchedule(schedule_class):
        def get_config(self):
            super().get_config()

        def __call__(self, step):
            step = tf.cast(step, tf.float32)
            return super().__call__(step)

    return LearningRateSchedule


learning_rates = {
    'piecewise_constant_decay': tf.keras.optimizers.schedules.PiecewiseConstantDecay,
    'triangular_cyclical_learning_rate': get_compatible_schedule_class(tfa.optimizers.TriangularCyclicalLearningRate),
    'triangular2_cyclical_learning_rate': get_compatible_schedule_class(tfa.optimizers.Triangular2CyclicalLearningRate)
}

losses = {
    'mse': tf.keras.losses.MeanSquaredError,
    'border_gaussian': BorderGaussianLoss,
    'asymmetrical_mse': AsymmetricalMSE,
    'binary_crossentropy': tf.keras.losses.BinaryCrossentropy,
    'categorical_crossentropy': tf.keras.losses.CategoricalCrossentropy,
    'sparse_categorical_crossentropy': tf.keras.losses.SparseCategoricalCrossentropy,
    'focal_crossentropy': tfa.losses.SigmoidFocalCrossEntropy
}

metrics = {
    'accuracy': tf.keras.metrics.Accuracy,
    'binary_accuracy': tf.keras.metrics.BinaryAccuracy,
    'mean_iou': tf.keras.metrics.MeanIoU,
    'mask_accuracy': MaskAccuracy,
    'border_accuracy': BorderAccuracy,
    'binary_mean_iou': BinaryMeanIoU
}

optimizers = {
    'sgd': tf.keras.optimizers.SGD,
    'adam': tf.keras.optimizers.Adam,
    'rectified_adam': tfa.optimizers.RectifiedAdam
}

callbacks = {
    'reduce_lr_on_plateau': tf.keras.callbacks.ReduceLROnPlateau
}

mask_selectors = {
    'mobilenet': src.mobilenet.mask_selector.MaskSelector,
    'deeplab': src.deeplab.mask_selector.MaskSelector
}

backbones = {
    'mobilenet': MobileNetV3Unet
}

base_models = {
    'MobileNet': tf.keras.applications.MobileNet,
    'MobileNetV2': tf.keras.applications.MobileNetV2,
    'MobileNetV3Small': tf.keras.applications.MobileNetV3Small,
    'MobileNetV3Large': tf.keras.applications.MobileNetV3Large
}
