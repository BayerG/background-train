import tensorflow as tf


def trapezoidal_integral(values, count):
    return 1 / (count - 1) * ((values[0] + values[-1]) / 2 + tf.reduce_sum(values[1:-1]))


class MaskAccuracy(tf.metrics.Metric):
    def __init__(self, name='mask_accuracy', threshold_count=11):
        super().__init__(name=name)
        self.threshold_count = threshold_count

        self.sum_accuracies = self.add_weight('sum_accuracies', (self.threshold_count, ))
        self.count = self.add_weight('count', [])
        self.reset_states()

    def reset_states(self):
        self.sum_accuracies.assign(tf.zeros(self.threshold_count))
        self.count.assign(0)

    def update_state(self, y_true, y_pred, sample_weight=None):
        thresholds = tf.linspace(0., 1., self.threshold_count)
        accuracies = tf.zeros(0)

        for i in range(len(thresholds)):
            y_true_binarized = tf.greater(y_true, thresholds[i])
            y_pred_binarized = tf.greater(y_pred, thresholds[i])
            accuracy = tf.equal(y_true_binarized, y_pred_binarized)
            accuracy = tf.cast(accuracy, tf.float32)
            accuracy = tf.reduce_mean(accuracy)
            accuracies = tf.concat((accuracies[:i], [accuracy], accuracies[i + 1:]), 0)

        self.sum_accuracies.assign_add(accuracies)
        self.count.assign_add(1)

    @tf.function
    def result(self):
        if self.count != 0:
            values = self.sum_accuracies / self.count
        else:
            values = self.sum_accuracies
        return trapezoidal_integral(values, self.threshold_count)


class BorderAccuracy(tf.metrics.Metric):
    def __init__(self, name='border_accuracy', width_count=11):
        super().__init__(name=name)
        self.width_count = width_count

        self.sum_accuracies = self.add_weight('sum_accuracies', (self.width_count, ))
        self.count = self.add_weight('count', [])
        self.reset_states()

    @staticmethod
    def in_border(tensor, border_width):
        return (0.5 - border_width / 2 <= tensor) & (tensor <= 0.5 + border_width / 2)

    def reset_states(self):
        self.sum_accuracies.assign(tf.zeros(self.width_count))
        self.count.assign(0)

    def update_state(self, y_true, y_pred, sample_weight=None):
        widths = tf.linspace(0., 1., self.width_count)
        accuracies = tf.zeros(self.width_count)

        for i in range(len(widths)):
            y_true_in_border = self.in_border(y_true, widths[i])
            y_pred_in_border = self.in_border(y_pred, widths[i])
            intersections = tf.cast((y_true_in_border == True) & (y_pred_in_border == True), tf.float32)
            unions = tf.stack((
                tf.cast((y_true_in_border == True) & (y_pred_in_border == True), tf.float32),
                tf.cast((y_true_in_border == True) & (y_pred_in_border == False), tf.float32),
                tf.cast((y_true_in_border == False) & (y_pred_in_border == True), tf.float32)
            ))
            accuracy = tf.math.divide_no_nan(tf.reduce_sum(intersections), tf.reduce_sum(unions))
            accuracies = tf.concat((accuracies[:i], [accuracy], accuracies[i + 1:]), 0)

        self.sum_accuracies.assign_add(accuracies)
        self.count.assign_add(1)

    @tf.function
    def result(self):
        if self.count != 0:
            values = self.sum_accuracies / self.count
        else:
            values = self.sum_accuracies
        return trapezoidal_integral(values, self.width_count)


class BinaryMeanIoU(tf.keras.metrics.MeanIoU):
    def __init__(self, num_classes, name=None, dtype=None, threshold=0.5):
        super().__init__(num_classes, name, dtype)
        self.threshold = threshold

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred_thresholded = tf.where(tf.less(y_pred, self.threshold), 0, 1)
        super().update_state(y_true, y_pred_thresholded, sample_weight)
