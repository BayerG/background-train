import tensorflow as tf


class BorderGaussianLoss(tf.keras.losses.Loss):
    def __init__(self, mean=0.5, std=0.25, scale=2.5, name='border_gaussian_loss'):
        super().__init__(name=name)
        self.mean = mean
        self.std = std
        self.scale = scale

    def call(self, y_true, y_pred):
        squared_error = (y_pred - y_true) ** 2
        symmetric_gaussian = 1 / tf.exp((squared_error - self.mean) ** 2 / (2 * self.std ** 2))
        result = self.scale * squared_error * symmetric_gaussian
        result = tf.reduce_mean(result)
        return result


class AsymmetricalMSE(tf.keras.losses.Loss):
    def __init__(self, alpha=0.5, name='asymmetrical_mse'):
        super().__init__(name=name)
        self.alpha = alpha

    def call(self, y_true, y_pred):
        result = (y_true - y_pred) ** 2 * tf.exp(self.alpha * (y_true - y_pred))
        result = tf.reduce_mean(result)
        return result
