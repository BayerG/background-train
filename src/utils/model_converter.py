import tensorflow as tf
import numpy as np
from pathlib import Path
from functools import partial
from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2
from time import monotonic as now

from src.utils.config import SingleConfig
from src.train import initialize_model
from src.utils.miscellaneous import backbones


class ModelConverter:
    def __init__(self):
        self._config = SingleConfig().get_part('model_converter')
        self._mode = self._config.mode
        self._checkpoint_model_path = Path(self._config.checkpoint_model_path)
        self._result_model_path = Path(self._config.result_model_path)
        self._backbone = self._config.backbone
        self._image_size = self._config.image_size
        self._image_channels = self._config.image_channels
        self._previous_frames = self._config.previous_frames
        self._mask_channels = self._config.mask_channels
        self._batch_size = 1

        self.__fabric = {
            'tflite': self._convert_to_tflite,
            'frozen': self._convert_to_frozen_model,
        }

    def _run(self):
        print(f'Start Model Converter in {self._mode} mode')
        print(f'Loading model from {str(self._checkpoint_model_path)}')
        start_time = now()
        model = initialize_model(self._checkpoint_model_path, self._image_size, self._image_channels,
                                 self._batch_size, self._previous_frames, self._mask_channels,
                                 backbone=backbones[self._backbone.type],
                                 **{key: self._backbone[key] for key in self._backbone if key != 'type'},
                                 down_model_training=False)
        self._convert(model)
        print(f'Conversion took {round(now() - start_time, 2)} seconds')

    def _convert(self, model):
        if self._mode not in self.__fabric.keys():
            print(f'Invalid converter mode: {self._mode}. Choose from {list(self.__fabric.keys())}')
            return
        partial(self.__fabric[self._mode], model)()

    def _convert_to_tflite(self, model):
        print('Converting to tflite model')
        converter = tf.lite.TFLiteConverter.from_keras_model(model)
        tflite_model = converter.convert()
        print(f'Saving model to {self._result_model_path}')
        self._result_model_path.parent.mkdir(parents=True, exist_ok=True)
        with open(self._result_model_path, 'wb') as file:
            file.write(tflite_model)

    def _convert_to_frozen_model(self, model):
        print('Converting to frozen model')
        full_model = tf.function(lambda inputs: model(inputs))
        full_model = full_model.get_concrete_function(tf.TensorSpec((1, int(self._image_size[1]),
                                                                     int(self._image_size[0]), 3), np.float32))
        frozen_func = convert_variables_to_constants_v2(full_model)
        frozen_func.graph.as_graph_def()
        print(f'Saving model to {self._result_model_path}')
        tf.io.write_graph(graph_or_graph_def=frozen_func.graph, logdir=str(self._result_model_path.parent),
                          name=str(self._result_model_path.name), as_text=False)

    @staticmethod
    def run():
        ModelConverter()._run()
