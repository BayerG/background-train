import tensorflow as tf


class Resizer:
    """
        Класс, предназначенный для изменения размеров изображений с опцией сохранения их пропорций.
        Реализован средствами tensorflow.
    """
    @staticmethod
    def downscale(image, size, aspect_ratio=True):
        """
        Изменение размера фотографии вниз с возможностью сохранения пропорций

        :param image: изображение типа tf.tensor или np.array, допускаются 3 или 4 канальные изображения
        :param size: размер, к которому нужно привести результирующее изображение, типа (x: int, y: int)
        :param aspect_ratio: сохранение пропорций, тип bool
        :return: уменьшенное изображение типа tf.tensor или np.array, dict с необходимой информацией для восстановления
        """
        shape_index_addition = 1 if len(image.shape) == 4 else 0
        original_size = image.shape[shape_index_addition:2+shape_index_addition]
        resized_image = tf.image.resize(image, size=size, preserve_aspect_ratio=aspect_ratio)

        h_offset = (size[0] - resized_image.shape[shape_index_addition]) // 2
        w_offset = (size[1] - resized_image.shape[shape_index_addition + 1]) // 2

        padded = tf.image.pad_to_bounding_box(resized_image, h_offset, w_offset, size[0], size[1])
        info = {
            'original_size': original_size,
            'resized_shape': resized_image.shape,
            'offsets': [h_offset, w_offset],
            'shape_index_addition': shape_index_addition
        }
        return padded, info

    @staticmethod
    def upscale(image, info):
        """
        Изменение размера фотографии вверх.
        :param image: изображение уменьшенного размера с использованием downscale
        :param info: dict с необходимой информацией для восстановления, полученный с использованием функции downscale
        :return: увеличенное изображение
        """
        cropped = tf.image.crop_to_bounding_box(image, info['offsets'][0], info['offsets'][1],
                                                info['resized_shape'][0 + info['shape_index_addition']],
                                                info['resized_shape'][1 + info['shape_index_addition']])
        resized = tf.image.resize(cropped, size=info['original_size'])
        return resized
