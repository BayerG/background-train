# For TensorFlow 2.4.0 and later.
import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa

from src.utils.config import SingleConfig

config = SingleConfig().augmentations


class Augmentations:
    def __init__(self, previous_frames, image_channels):
        self.previous_frames = previous_frames
        self.image_channels = image_channels

        if config.random_seed is not None:
            self.generator = tf.random.Generator.from_seed(config.random_seed)
        else:
            self.generator = tf.random.Generator.from_non_deterministic_state()

    def __call__(self, image, mask):
        # Unsqueeze if not in batch.
        if len(image.shape) < 4:
            image = tf.expand_dims(image, 0)
            mask = tf.expand_dims(mask, 0)
            expanded_dims = True
        else:
            expanded_dims = False

        # Generate random flags for previous mask augmentations.
        probability_values = self.generator.uniform((image.shape[0],))
        change_flags = not (probability_values < config.previous_frames.no_change_probability)
        change_flags = tf.cast(change_flags, tf.float32)
        not_empty_flags = not ((config.previous_frames.no_change_probability <= probability_values)
                               & (probability_values < config.previous_frames.no_change_probability
                                    + config.previous_frames.empty_probability))
        not_empty_flags = tf.cast(not_empty_flags, tf.float32)

        if 'brightness_range' in config \
                    and config.brightness_range is not None:
            # Brightness (image only), separately for foreground and background.
            if 'foreground_gradient' in config.brightness_range \
                    and config.brightness_range.foregroun_gradient is not None:
                foreground_brightness_deltas = self.generator.uniform(
                    (image.shape[0], 2, 2, 1),
                    *config.brightness_range.foreground_gradient
                )
                foreground_brightness_deltas = tf.image.resize(foreground_brightness_deltas, (image.shape[1], image.shape[2]),
                                                               tf.image.ResizeMethod.NEAREST_NEIGHBOR)
            else:
                foreground_brightness_deltas = tf.zeros((image.shape[0], 2, 2, 1))

            if 'foreground_brightness' in config.brightness_range \
                    and config.brightness_range.foreground_brightness is not None:
                foreground_brightness_deltas += self.generator.uniform(
                    [],
                    *config.brightness_range.foreground_brightness
                )

            if 'background_gradient' in config.brightness_range \
                    and config.brightness_range.background_gradient is not None:
                background_brightness_deltas = self.generator.uniform(
                    (image.shape[0], 2, 2, 1),
                    *config.brightness_range.background_gradient
                )
                background_brightness_deltas = tf.image.resize(background_brightness_deltas, (image.shape[1], image.shape[2]),
                                                               tf.image.ResizeMethod.NEAREST_NEIGHBOR)
                background_brightness_deltas += self.generator.uniform([], *config.brightness_range.background_brightness)
            else:
                background_brightness_deltas = tf.zeros((image.shape[0], 2, 2, 1))

            brightness_deltas_composition = foreground_brightness_deltas * mask[:, :, :, :1] \
                                            + background_brightness_deltas * (1 - mask[:, :, :, :1])
            if not self.previous_frames:
                image = self.adjust_brightness(image, brightness_deltas_composition)
            else:
                image = tf.concat((
                    self.adjust_brightness(image[:, :, :, :self.image_channels], brightness_deltas_composition),
                    image[:, :, :, self.image_channels:]
                ), axis=3)

            # Brightness (image only), for whole image.
            if 'whole_gradient' in config.brightness_range \
                    and config.brightness_range is not None:
                brightness_deltas = self.generator.uniform(
                    (image.shape[0], 2, 2, 1),
                    *config.brightness_range.whole_gradient
                )
                brightness_deltas = tf.image.resize(brightness_deltas, (image.shape[1], image.shape[2]),
                                                    tf.image.ResizeMethod.NEAREST_NEIGHBOR)
                brightness_deltas += self.generator.uniform([], *config.brightness_range.whole_brightness)

                if not self.previous_frames:
                    image = self.adjust_brightness(image, brightness_deltas)
                else:
                    image = tf.concat((
                        self.adjust_brightness(image[:, :, :, :self.image_channels], brightness_deltas),
                        image[:, :, :, self.image_channels:]
                    ), axis=3)

        # Flip.
        hflip_random = self.generator.uniform([])
        vflip_random = self.generator.uniform([])
        if hflip_random < config.hflip_probability:
            image = tf.image.flip_left_right(image)
            mask = tf.image.flip_left_right(mask)
        if vflip_random < config.vflip_probability:
            image = tf.image.flip_up_down(image)
            mask = tf.image.flip_up_down(mask)

        # Offset.
        offsets = self.generate_shift_offsets(
            image.shape[0],
            config.horizontal_offset_range,
            config.vertical_offset_range
        )
        image = self.crop_image(image, offsets)
        mask = self.crop_image(mask, offsets)
        if self.previous_frames:
            offsets = self.generate_shift_offsets(
                image.shape[0],
                config.previous_frames.horizontal_offset_range,
                config.previous_frames.vertical_offset_range
            )
            offsets = offsets * change_flags
            image = tf.concat((
                image[:, :, :, :self.image_channels],
                self.crop_image(image[:, :, :, self.image_channels:], offsets)
            ), axis=3)

        # Rotate.
        angles = self.generate_angles(image.shape[0], config.angle_range)
        image = tfa.image.rotate(image, angles)
        mask = tfa.image.rotate(mask, angles)
        if self.previous_frames:
            angles = self.generate_angles(image.shape[0], config.previous_frames.angle_range)
            angles = angles * change_flags
            image = tf.concat((
                image[:, :, :, :self.image_channels],
                tfa.image.rotate(image[:, :, :, self.image_channels:], angles)
            ), axis=3)

        # Scale.
        offsets = self.generate_scale_offsets(image.shape[0], config.scale_range)
        image = self.crop_image(image, offsets)
        mask = self.crop_image(mask, offsets)
        if self.previous_frames:
            offsets = self.generate_scale_offsets(image.shape[0], config.scale_range)
            offsets = offsets * change_flags
            image = tf.concat((
                image[:, :, :, :self.image_channels],
                self.crop_image(image[:, :, :, self.image_channels:], offsets)
            ), axis=3)

        # Empty previous mask
        if self.previous_frames:
            image = tf.concat((
                image[:, :, :, :self.image_channels],
                image[:, :, :, self.image_channels:] * not_empty_flags
            ), axis=3)

        # Squeeze back if unsqueezed.
        if expanded_dims:
            image = tf.squeeze(image, 0)
            mask = tf.squeeze(mask, 0)

        return image, mask

    @staticmethod
    def adjust_brightness(image, brightness_deltas):
        positive_brightness_deltas = tf.where(tf.greater(brightness_deltas, 0), brightness_deltas, 0)
        negative_brightness_deltas = tf.where(tf.less(brightness_deltas, 0), brightness_deltas, 0)
        image = 255 - (255 - image) * tf.maximum(1 - positive_brightness_deltas, 0)
        image = image * tf.maximum(1 + negative_brightness_deltas, 0)
        return image

    def generate_shift_offsets(self, length, horizontal_offset_range, vertical_offset_range):
        offsets = self.generator.uniform(
            (length, 2),
            [vertical_offset_range[0], horizontal_offset_range[0]],
            [vertical_offset_range[1], horizontal_offset_range[1]]
        )
        offsets = tf.repeat(offsets, 2, 0)
        offsets = tf.reshape(offsets, (length, 4))
        return offsets

    def generate_angles(self, length, angle_range):
        min_angle_radian = angle_range[0] * np.pi / 180
        max_angle_radian = angle_range[1] * np.pi / 180
        angles = self.generator.uniform((length,), min_angle_radian, max_angle_radian)
        return angles

    def generate_scale_offsets(self, length, scale_range):
        scales = self.generator.uniform((length,), *scale_range)
        offsets = ((1 / scales) - 1) / 2
        offsets = tf.repeat(offsets, 4)
        offsets = tf.reshape(offsets, (length, 4))
        offsets *= tf.repeat([[-1., -1., 1., 1.]], length, 0)
        return offsets

    @staticmethod
    def crop_image(image, offsets):
        boxes = tf.repeat([[0., 0., 1., 1.]], image.shape[0], 0)
        boxes += offsets
        indices = tf.range(image.shape[0])
        crop_size = (image.shape[1], image.shape[2])
        image = tf.image.crop_and_resize(image, boxes, indices, crop_size,
                                         tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        return image
