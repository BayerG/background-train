import numpy as np

from src.utils.config import SingleConfig
from src.utils.inferencer import BaseInferencer

from time import monotonic as now


class ConvertedModelTester:
    def __init__(self):
        self._config = SingleConfig().get_part('converted_model_tester')
        self._participants = self._config.participants
        self._path_to_models = self._config.path_to_models
        self._models = [BaseInferencer.get_instance(p, self._path_to_models[p]) for p in self._participants]
        self._image_size = self._config.image_size

    def compare(self):
        sample = np.random.uniform(low=0, high=255, size=(1, self._image_size[1], self._image_size[0], 3))\
            .astype(np.float32)

        results = list()
        for model in self._models:
            result = model.run(sample)
            results.append(self._convert_to_01(result))

        if len(results) > 0:
            answer = results[0]
            answer_name = self._models[0].get_name()
        else:
            print('No results! Check participants in config!')
            return

        for i, cur_res in enumerate(results[1:]):
            print(f'{answer_name} == {self._models[i + 1].get_name()}: {np.all(cur_res == answer)}')

        print('Non_zero_elements:', np.count_nonzero(answer))

    def calculate_time(self):
        top = list()
        for model in self._models:
            total, per_iteration = self._one_iteration(model)
            top.append({'per_iteration': per_iteration, 'name': model.get_name()})
            print(f'{model.get_name()}: total - {round(total, 3)} sec, per_iteration - {round(per_iteration, 3)} sec')

        top = sorted(top, key=lambda x: x['per_iteration'])
        print(f'top results: {[x["name"] for x in top]}')

    def _one_iteration(self, model, count=1000):
        start_time = now()
        for _ in range(count):
            sample = np.random.uniform(low=0, high=255, size=(1, self._image_size[1], self._image_size[0], 3))\
                .astype(np.float32)
            model.run(sample)
        total = now() - start_time
        return total, total / count

    @staticmethod
    def _convert_to_01(mask, threshold=0.5):
        mask = mask > float(threshold)
        return mask.astype(np.uint8)

    @staticmethod
    def run():
        cmt = ConvertedModelTester()
        cmt.compare()
        cmt.calculate_time()
