import tensorflow as tf
import numpy as np
from openvino.inference_engine import IECore

from src.train import initialize_model
from src.utils.config import SingleConfig
from src.utils.miscellaneous import backbones


class BaseInferencer:
    def __init__(self, path_to_model):
        self._path_to_model = path_to_model

    def run(self, image):
        pass

    def get_name(self):
        return 'BaseInferencer'

    @staticmethod
    def get_instance(model_type, path_to_model):
        choices = {
            'checkpoint': CheckpointInferencer,
            'tflite': TfliteInferencer,
            'frozen': FrozenInferencer,
            'openvino': OpenVinoInferencer,
        }
        if model_type not in choices.keys():
            raise ValueError(f'model type not in {list(choices.keys())}')
        return choices[model_type](path_to_model)


class CheckpointInferencer(BaseInferencer):
    def __init__(self, path_to_model):
        super(CheckpointInferencer, self).__init__(path_to_model)
        config = SingleConfig().get_part('model_converter')
        self.model = initialize_model(
            self._path_to_model,
            config.image_size,
            config.image_channels,
            1,
            config.previous_frames,
            config.mask_channels,
            backbone=backbones[config.backbone.type],
            **{key: config.backbone[key] for key in config.backbone if key != 'type'},
            down_model_training=False
        )

    def run(self, image):
        return self.model.predict(image).squeeze()

    def get_name(self):
        return 'checkpoint'


class TfliteInferencer(BaseInferencer):
    def __init__(self, path_to_model):
        super(TfliteInferencer, self).__init__(path_to_model)
        self._interpreter = tf.lite.Interpreter(model_path=self._path_to_model)
        self._input_details = self._interpreter.get_input_details()
        self._output_details = self._interpreter.get_output_details()
        self._interpreter.allocate_tensors()

    def run(self, image):
        self._interpreter.set_tensor(self._input_details[0]['index'], image)
        self._interpreter.invoke()
        mask = self._interpreter.get_tensor(self._output_details[0]['index'])
        mask = mask.squeeze(0)
        mask = mask[:, :, -1]
        return mask

    def get_name(self):
        return 'tflite'


class OpenVinoInferencer(BaseInferencer):
    def __init__(self, path_to_model):
        super(OpenVinoInferencer, self).__init__(path_to_model)
        ie = IECore()
        network = ie.read_network(model=self._path_to_model, weights=self._path_to_model.replace('.xml', '.bin'))

        self.executable_network = ie.load_network(network, device_name='CPU', num_requests=1)
        self.input_blob_name = next(iter(self.executable_network.input_info))
        self.output_blob_name = next(iter(self.executable_network.outputs))
        self.sigmoid = tf.keras.activations.sigmoid

    def run(self, image: np.ndarray):
        input_image = image.transpose((0, 3, 1, 2))
        result = self.executable_network.infer({self.input_blob_name: input_image})
        result = result[self.output_blob_name]
        result = self.sigmoid(result)
        result = result.numpy()
        return result.squeeze()

    def get_name(self):
        return 'openvino'


class FrozenInferencer(BaseInferencer):
    def __init__(self, path_to_model):
        super(FrozenInferencer, self).__init__(path_to_model)
        devices = {'CPU': 1, 'GPU': 0}
        tf_config = tf.compat.v1.ConfigProto(device_count=devices)
        self.sess = tf.compat.v1.Session(config=tf_config)
        with tf.io.gfile.GFile(self._path_to_model, 'rb') as f:
            graph_def = tf.compat.v1.GraphDef()
            graph_def.ParseFromString(f.read())
            self.sess.graph.as_default()
            tf.import_graph_def(graph_def)
            operations = tf.compat.v1.get_default_graph().get_operations()
            input_name = operations[0].name if len(operations) > 0 else None
            output_name = operations[-1].name if len(operations) > 0 else None

        self.tensor_output = self.sess.graph.get_tensor_by_name(f'{output_name}:0')
        self.tensor_input = self.sess.graph.get_tensor_by_name(f'{input_name}:0')

    def run(self, image):
        with self.sess.graph.as_default():
            result = self.sess.run(self.tensor_output, {self.tensor_input: image})
            result = result.squeeze()
        return result

    def get_name(self):
        return 'frozen'
