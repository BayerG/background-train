from typing import *

import numpy as np
import cv2
import tensorflow as tf
from tf_bodypix.bodypix_js_utils.part_channels import PART_CHANNELS

from src.utils.miscellaneous import between
from src.utils.config import SingleConfig

config = SingleConfig().filter


class SegmentationDataset:
    def __init__(self, objects_data, image_size, preserve_aspect_ratio, image_channels, mask_channels,
                 background_channels, previous_frames):
        self.objects_data = objects_data
        self.image_size = image_size  # (height, width)
        self.preserve_aspect_ratio = preserve_aspect_ratio
        self.image_channels = image_channels
        self.mask_channels = mask_channels
        self.background_channels = background_channels
        self.previous_frames = previous_frames

        self.image_mask_filenames = []

    @staticmethod
    def load_dataset(
            objects_data: list,
            image_size: Tuple[int, int],  # (height, width)
            preserve_aspect_ratio,
            image_channels: int,
            mask_channels: int,
            background_channels: int,
            previous_frames: bool
    ):
        dataset = SegmentationDataset(objects_data, image_size, preserve_aspect_ratio, image_channels, mask_channels,
                                      background_channels, previous_frames)
        dataset.image_mask_filenames = []

        for object_data in objects_data:
            if 'original_instance_count_probabilities' in config \
                    and config.original_instance_count_probabilities is not None \
                    and str(len(object_data['masks'])) in config.original_instance_count_probabilities \
                    and tf.random.uniform(()) \
                        >= config.original_instance_count_probabilities[str(len(object_data['masks']))]:
                continue

            mask_filenames, discard = dataset.fill_mask_filenames(object_data['masks'])
            if discard:
                continue

            if previous_frames:
                if 'previous_frame_filename' in object_data:
                    previous_mask_filenames, discard = dataset.fill_mask_filenames(object_data['previous_masks'])
                else:
                    continue
                if discard:
                    continue

            if 'result_instance_count_probabilities' in config \
                    and config.result_instance_count_probabilities is not None \
                    and str(len(mask_filenames)) in config.result_instance_count_probabilities \
                    and tf.random.uniform(()) >= config.result_instance_count_probabilities[str(len(mask_filenames))]:
                continue

            image_filename = object_data['image_filename']
            if 'background' in object_data:
                background_filename = object_data['background']['image_filename']
            else:
                background_filename = None
            if not previous_frames:
                dataset.image_mask_filenames.append([image_filename, mask_filenames, background_filename])
            else:
                dataset.image_mask_filenames.append([image_filename, mask_filenames, previous_mask_filenames,
                                                     background_filename])

        generated_dataset = tf.data.Dataset.from_generator(
            dataset.generator,
            output_signature=(
                tf.TensorSpec(
                    tuple(image_size) + (image_channels + int(previous_frames) * mask_channels,),
                    dtype=tf.float32
                ),
                tf.TensorSpec(
                    tuple(image_size) + (mask_channels,),
                    dtype=tf.float32
                )
            )
        )
        dataset_length = len(dataset.image_mask_filenames)

        return generated_dataset, dataset_length

    @staticmethod
    def fill_mask_filenames(masks_data):
        discard = False
        mask_filenames = []

        for mask_data in masks_data:
            in_bounds = True

            if 'score_range' in config.instance_filter \
                    and config.instance_filter.score_range is not None \
                    and not between(
                        mask_data['score'], config.instance_filter.score_range
                    ):
                in_bounds = False

            if 'mask_area_range' in config.instance_filter \
                    and config.instance_filter.mask_area_range is not None \
                    and not between(
                        mask_data['instance_area'],
                        config.instance_filter.mask_area_range,
                        mask_data['mask_width'] * mask_data['mask_height']
                    ):
                in_bounds = False

            if 'box' in config.instance_filter:
                left, top, right, bottom = mask_data['box']
                width, height = right - left, bottom - top
                aspect_ratio = width / height
                box_area = width * height
                bound_names = ['left', 'top', 'right', 'bottom', 'width', 'height', 'aspect_ratio', 'box_area']
                for bound, bound_range, whole in zip([
                    eval(bound_name) for bound_name in bound_names
                ], [
                    config.instance_filter.box[bound_name]
                    if bound_name in config.instance_filter.box else [None, None]
                    for bound_name in bound_names
                ], [
                    mask_data['mask_width'],
                    mask_data['mask_height'],
                    mask_data['mask_width'],
                    mask_data['mask_height'],
                    mask_data['mask_width'],
                    mask_data['mask_height'],
                    1,
                    mask_data['mask_height'] * mask_data['mask_width']
                ]):
                    if bound_range is not None \
                            and not between(bound, bound_range, whole):
                        in_bounds = False
                        break

            if 'body_parts' in config.instance_filter:
                for part_channel in PART_CHANNELS:
                    if part_channel in config.instance_filter.body_parts \
                            and not between(
                                mask_data[part_channel],
                                config.instance_filter.body_parts[part_channel],
                                mask_data['mask_height'] * mask_data['mask_width']
                            ):
                        in_bounds = False

            if not in_bounds:
                if not config.discard_if_any_filtered:
                    continue
                else:
                    discard = True
                    break

            mask_filenames.append(mask_data['mask_filename'])

        return mask_filenames, discard

    def generator(self):
        for i, filenames in enumerate(self.image_mask_filenames):
            if not self.previous_frames:
                image_filename, mask_filenames, background_filename = filenames
            else:
                image_filename, mask_filenames, previous_mask_filenames, background_filename = filenames
            image = tf.io.decode_jpeg(tf.io.read_file(image_filename), channels=self.image_channels)
            if not self.preserve_aspect_ratio:
                image = tf.image.resize(image, self.image_size)
            else:
                image = tf.image.resize_with_pad(image, *self.image_size)
            mask = self.merge_instance_masks(mask_filenames, self.image_size, self.preserve_aspect_ratio,
                                             self.mask_channels, config.instance_filter.mask_threshold,
                                             config.instance_filter.leave_one_component)
            if self.previous_frames:
                previous_mask = self.merge_instance_masks(previous_mask_filenames, self.image_size,
                                                          self.preserve_aspect_ratio,
                                                          self.mask_channels, config.instance_filter.mask_threshold,
                                                          config.instance_filter.leave_one_component)
            mask /= 255
            if background_filename is not None:
                background = tf.io.decode_jpeg(tf.io.read_file(background_filename), channels=self.background_channels)
                if not self.preserve_aspect_ratio:
                    background = tf.image.resize(background, self.image_size)
                else:
                    background = tf.image.resize_with_pad(background, *self.image_size)

                image = image * mask + background * (1 - mask)
            if self.previous_frames:
                image = tf.concat((image, previous_mask), axis=2)

            yield image, mask

    @staticmethod
    def merge_instance_masks(mask_filenames, image_size, preserve_aspect_ratio, mask_channels, mask_threshold,
                             leave_one_component):
        instance_masks = tf.zeros((max(len(mask_filenames), 1), *image_size, mask_channels), dtype=tf.float32)

        for i, mask_filename in enumerate(mask_filenames):
            instance_mask = tf.io.decode_jpeg(tf.io.read_file(mask_filename), channels=mask_channels)
            if not preserve_aspect_ratio:
                instance_mask = tf.image.resize(instance_mask, image_size)
            else:
                instance_mask = tf.image.resize_with_pad(instance_mask, *image_size)

            if mask_threshold is not None:
                mask_threshold = mask_threshold \
                    if isinstance(mask_threshold, int) \
                    else int(mask_threshold * 255)
                instance_mask = tf.where(tf.less(instance_mask, mask_threshold), 0, 255)

            if leave_one_component:
                instance_mask = SegmentationDataset.leave_one_component(instance_mask)

            instance_masks = tf.concat((instance_masks[:i], [instance_mask], instance_masks[i + 1:]), axis=0)

        mask = tf.reduce_max(instance_masks, axis=0)
        return mask

    @staticmethod
    def leave_one_component(mask):
        mask = tf.cast(mask, tf.uint8).numpy()
        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        contours_lengths = np.array([len(contour) for contour in contours])
        sorted = np.argsort(contours_lengths)
        contours = [contours[sorted[i]] for i in range(len(contours))]
        mask = cv2.drawContours(mask, contours[:-1], -1, (0, 0, 0), -1)
        mask = tf.cast(mask, tf.float32)
        return mask

    @staticmethod
    def scale_image(image, scale):
        scaled_image = tf.image.resize(image, (int(image.shape[0] * scale), int(image.shape[1] * scale)))
        scaled_image = tf.image.resize_with_crop_or_pad(scaled_image, image.shape[0], image.shape[1])
        scaled_image = tf.cast(scaled_image, image.dtype)
        return scaled_image
