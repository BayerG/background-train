from pathlib import Path
import os
import json
import random
from datetime import datetime
import tensorflow as tf

from src.utils.dataset import SegmentationDataset
from src.utils.augmentations import Augmentations
from src.deeplab.mask_selector import MaskSelector
from src.utils.miscellaneous import experiments_folder, cache_filename, LearningRateLogger, learning_rates, losses, \
    metrics, optimizers, callbacks, backbones, mask_selectors
from src.utils.config import SingleConfig

config = SingleConfig().train


def train():
    random.seed(config.random_seed)

    print('Initializing experiment')
    experiment_path = initialize_experiment()

    print('Logging config')
    file_writer = tf.summary.create_file_writer(str(experiment_path / 'config'))
    with file_writer.as_default():
        with open('config.yaml') as file:
            config_text = ''.join('\t' + line for line in file.read().splitlines(True))
            tf.summary.text('config', config_text, step=0)

    print('Loading train dataset')
    train_dataset, train_dataset_length = get_dataset(
        config.dataset.train.json,
        experiment_path,
        config.dataset.train.augmentations
    )
    print('Loading test dataset')
    test_dataset, _ = get_dataset(
        config.dataset.test.json,
        experiment_path,
        config.dataset.test.augmentations,
    )

    train_steps = train_dataset_length / config.dataset.batch_size
    learning_rate = get_learning_rate(train_steps)
    optimizer = optimizers[config.optimizer.type](
        learning_rate=learning_rate,
        **{key: config.optimizer[key] for key in config.optimizer if key != 'type'}
    )
    loss = losses[config.loss.type](**{key: config.loss[key] for key in config.loss if key != 'type'})
    metric_objects = []
    for metric_info in config.metrics:
        metric = metrics[metric_info.type](**{key: metric_info[key] for key in metric_info if key != 'type'})
        metric_objects.append(metric)

    print('Initializing model')
    model = initialize_model(
        config.checkpoint,
        config.dataset.image_size,
        config.dataset.image_channels,
        config.dataset.batch_size,
        config.dataset.previous_frames,
        config.dataset.mask_channels,
        backbone=backbones[config.backbone.type],
        **{key: config.backbone[key] for key in config.backbone if key != 'type'},
        down_model_training=config.down_model_training
    )

    if config.down_model_trainable:
        model.backbone.down_model.trainable = False

    model.compile(optimizer=optimizer, loss=loss, metrics=metric_objects)
    if config.reset_learning_rate:
        model.optimizer.learning_rate.assign(config.learning_rate)

    callback_objects = get_callbacks(experiment_path)

    model.fit(
        x=train_dataset,
        epochs=config.epochs,
        validation_data=test_dataset,
        callbacks=callback_objects,
        verbose=config.verbose
    )

    print('Done')


def initialize_experiment():
    experiment_path = Path(experiments_folder, datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
    print(f'Saving experiment to {experiment_path}')
    if not experiment_path.exists():
        os.makedirs(experiment_path, exist_ok=True)
    SingleConfig().copy_to(experiment_path / 'config.yaml')
    return experiment_path


def get_dataset(dataset_json, experiment_path, augmentations):
    with open(dataset_json) as file:
        objects_data = json.load(file)
    if config.dataset.shuffle:
        random.shuffle(objects_data)
    dataset, dataset_length = SegmentationDataset.load_dataset(
        objects_data,
        config.dataset.image_size[::-1],
        config.dataset.preserve_aspect_ratio,
        config.dataset.image_channels,
        config.dataset.mask_channels,
        config.dataset.background_channels,
        config.dataset.previous_frames
    )

    if config.dataset.cache:
        cache_path = os.path.join(experiment_path, cache_filename)
        cache_dirname = os.path.dirname(cache_path)
        if cache_dirname != '' and not os.path.exists(cache_dirname):
            os.makedirs(cache_dirname)
        print(f'Caching dataset in {cache_path}')
        dataset = dataset.cache(cache_path)
    if augmentations:
        dataset = dataset.map(Augmentations(config.dataset.previous_frames, config.dataset.image_channels))
    dataset = dataset.batch(config.dataset.batch_size)
    return dataset, dataset_length


def get_learning_rate(steps):
    if isinstance(config.learning_rate, float):
        learning_rate = config.learning_rate
    elif isinstance(config.learning_rate, dict):
        learning_rate_type = list(config.learning_rate.keys())[0]
        learning_rate_options = config.learning_rate[learning_rate_type]
        if 'epoch_step_size' in learning_rate_options:
            epoch_step_size = learning_rate_options.pop('epoch_step_size')
            learning_rate_options['step_size'] = epoch_step_size * steps
        if 'epoch_boundaries' in learning_rate_options:
            epoch_boundaries = learning_rate_options.pop('epoch_boundaries')
            learning_rate_options['boundaries'] \
                = list(map(lambda epoch_boundary: epoch_boundary * steps, epoch_boundaries))
        learning_rate = learning_rates[learning_rate_type](**learning_rate_options)
    else:
        raise ValueError('Incorrect learning rate')
    return learning_rate


def initialize_model(mask_selector, checkpoint, image_size, image_channels, batch_size, previous_frames, mask_channels, *args,
                     **kwargs):
    model = mask_selectors[mask_selector](*args, **kwargs)
    if checkpoint is not None:
        print(f'Loading weights from {checkpoint}')
        model.predict(tf.random.uniform((
            batch_size,
            *image_size[::-1],
            image_channels + int(previous_frames) * mask_channels
        )))
        if not str(checkpoint).endswith('.ckpt'):
            checkpoint = tf.train.latest_checkpoint(checkpoint)
        model.load_weights(checkpoint)
    return model


def get_callbacks(experiment_path: Path):
    lr_logger_callback = LearningRateLogger()

    checkpoint_path = str(experiment_path / 'checkpoint' / '{epoch}.ckpt')
    checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path, verbose=1)

    logs_path = experiment_path / 'logs'
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logs_path, update_freq='batch')

    callback_objects = []
    if config.callbacks is not None:
        callback_objects += [callbacks[callback_name](**callback_options)
                             for callback_name, callback_options in config.callbacks.items()]
    callback_objects += [lr_logger_callback, checkpoint_callback, tensorboard_callback]

    return callback_objects
