import numpy as np
import pytest
from src.utils.resizer import Resizer


@pytest.mark.parametrize('size, aspect_ratio',
                         [
                             ((224, 224), True),
                             ((224, 224), False),
                             ((172, 196), True),
                             ((172, 196), False)
                         ])
def test_downscale_on_image(size, aspect_ratio):
    image = np.ones(shape=(720, 480, 3))

    downscaled, _ = Resizer.downscale(image, size=size, aspect_ratio=aspect_ratio)

    right_shape = (size[0], size[1], 3)
    assert right_shape == downscaled.shape


@pytest.mark.parametrize('size, aspect_ratio',
                         [
                             ((224, 224), True),
                             ((224, 224), False),
                             ((172, 196), True),
                             ((172, 196), False)
                         ])
def test_downscale_on_batch(size, aspect_ratio):
    image = np.ones(shape=(16, 720, 480, 3))

    downscaled, _ = Resizer.downscale(image, size=size, aspect_ratio=aspect_ratio)

    right_shape = (16, size[0], size[1], 3)
    assert right_shape == downscaled.shape


def test_upscale_on_image():
    image = np.ones(shape=(224, 224, 3))
    info = {
            'original_size': (720, 480),
            'resized_shape': (224, 178, 3),
            'offsets': [0, 23],
            'shape_index_addition': 0
        }
    result = Resizer.upscale(image, info)

    assert result.shape == (720, 480, 3)


def test_upscale_on_batch():
    image = np.ones(shape=(16, 224, 224, 3))
    info = {
            'original_size': (720, 480),
            'resized_shape': (224, 178, 3),
            'offsets': [0, 23],
            'shape_index_addition': 1
        }
    result = Resizer.upscale(image, info)

    assert result.shape == (16, 720, 480, 3)


@pytest.mark.parametrize('aspect_ratio', [True, False])
def test_resizer_on_image(aspect_ratio):
    image = np.ones(shape=(724, 345, 3))

    downscaled, info = Resizer.downscale(image, size=(224, 224), aspect_ratio=aspect_ratio)
    upscaled = Resizer.upscale(downscaled, info)

    assert np.all(upscaled == image)


@pytest.mark.parametrize('aspect_ratio', [True, False])
def test_resizer_on_batch(aspect_ratio):
    image = np.ones(shape=(16, 724, 345, 3))

    downscaled, info = Resizer.downscale(image, size=(224, 224), aspect_ratio=aspect_ratio)
    upscaled = Resizer.upscale(downscaled, info)

    assert np.all(upscaled == image)
